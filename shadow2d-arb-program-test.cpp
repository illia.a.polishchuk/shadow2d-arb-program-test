#include "glad/glad.h"
#include <GLFW/glfw3.h>
#include <array>
#include <iomanip>
#include <iostream>
#include <string.h>
#include <thread>
#include <vector>

static const int g_windowWidth = 1024;
static const int g_windowHeight = 768;

GLFWwindow *
CreateWindowAndOpengContext(int width, int height)
{
   glfwInit();
   glfwSetErrorCallback([](int, const char *err_str) {
      std::cout << "GLFW Error: " << err_str << std::endl;
   });

   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 1);
   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

   GLFWwindow *window =
       glfwCreateWindow(width, height, "Depth precision test", NULL, NULL);

   if (window == NULL) {
      std::cerr << "Failed to create GLFW window" << std::endl;
      std::terminate();
   }

   glfwMakeContextCurrent(window);

   if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
      std::cerr << "Failed to initialize GLAD" << std::endl;
      std::terminate();
   }

   return window;
}

const char *g_arb_vertex_program_str =
    R"(!!ARBvp1.0
# cgc version 1.5.0019, build date Feb 22 2007 06:27:05
# command line args: -q -profile arbvp1 -entry main NumTemps=256 MaxInstructions=16384 MaxAddressRegs=1 MaxLocalParams=2048
# source file: core/programs/PostEffect_Bloom_vp.cg
#vendor NVIDIA Corporation
#version 1.5.0.19
#profile arbvp1
#program main
#semantic main.worldViewProj
#var float4 position : $vin.POSITION : POSITION : 0 : 1
#var float3 color : $vin.COLOR : COLOR0 : 1 : 1
#var float2 uv0 : $vin.TEXCOORD0 : TEXCOORD0 : 2 : 1
#var float2 uv1 : $vin.TEXCOORD1 : TEXCOORD1 : 3 : 1
#var float4 oPosition : $vout.POSITION : HPOS : 4 : 1
#var float3 oColor : $vout.COLOR : COL0 : 5 : 1
#var float2 oUv0 : $vout.TEXCOORD0 : TEX0 : 6 : 1
#var float2 oUv1 : $vout.TEXCOORD1 : TEX1 : 7 : 1
#var float4x4 worldViewProj :  : c[1], 4 : 8 : 1
PARAM c[5] = { program.local[0..4] };
MOV result.color.xyz, vertex.color;
MOV result.texcoord[0].xy, vertex.texcoord[0];
MOV result.texcoord[1].xy, vertex.texcoord[1];
DP4 result.position.w, vertex.position, c[4];
DP4 result.position.z, vertex.position, c[3];
DP4 result.position.y, vertex.position, c[2];
DP4 result.position.x, vertex.position, c[1];
END
# 7 instructions, 0 R-regs
)";

const char *g_arb_fragment_program_str =
    R"(!!ARBfp1.0
OPTION ARB_fragment_program_shadow;
# cgc version 1.5.0019, build date Feb 22 2007 06:27:05
# command line args: -q -profile arbfp1 -entry main NumTemps=256 NumInstructionSlots=1024 MaxLocalParams=2048 NumTexInstructionSlots=1024 NumMathInstructionSlots=1024 MaxTexIndirections=1024 MaxDrawBuffers=8
# source file: core/programs/PostEffect_Bloom_fp.cg
#vendor NVIDIA Corporation
#version 1.5.0.19
#profile arbfp1
#program main
#semantic main.blurTex : TEXUNIT0
#semantic main.screenTex : TEXUNIT1
#var float4 color : $vin.COLOR0 :  : 1 : 0
#var float3 uv0 : $vin.TEXCOORD0 : TEX0 : 2 : 1
#var float3 uv1 : $vin.TEXCOORD1 :  : 3 : 0
#var float4 oColor : $vout.COLOR : COL : 4 : 1
#var sampler2DSHADOW blurTex : TEXUNIT0 : texunit 0 : 5 : 1
#var samplerRECT screenTex : TEXUNIT1 : texunit 1 : 6 : 0
TEX result.color, fragment.texcoord[0], texture[0], SHADOW2D;
END
# 1 instructions, 0 R-regs
)";

int
main()
{
   GLFWwindow *window =
       CreateWindowAndOpengContext(g_windowWidth, g_windowHeight);

   int width = 0, height = 0;
   glfwGetFramebufferSize(window, &width, &height);
   glViewport(0, 0, width, height);

   glEnable(GL_TEXTURE_2D);
   glEnable(GL_VERTEX_PROGRAM_ARB);
   glEnable(GL_FRAGMENT_PROGRAM_ARB);
   glEnable(GL_MULTISAMPLE);

   GLuint texture_rgba;
   glGenTextures(1, &texture_rgba);
   glBindTexture(GL_TEXTURE_2D, texture_rgba);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, NULL);

   // Fill texture with a red color from active framebuffer
   glClearColor(0.f, 1.f, 0.f, 1.f);
   glClear(GL_COLOR_BUFFER_BIT);
   glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);

   GLuint texture_depth;
   glGenTextures(1, &texture_depth);
   glBindTexture(GL_TEXTURE_2D, texture_depth);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0,
                GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);

   // Fill texture with a red color from active framebuffer
   glClearColor(0.f, 1.f, 0.f, 1.f);
   glClear(GL_COLOR_BUFFER_BIT);
   glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);

   GLuint progid_vert;
   glGenProgramsARB(1, &progid_vert);
   glBindProgramARB(GL_VERTEX_PROGRAM_ARB, progid_vert);
   glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
                      strlen(g_arb_vertex_program_str),
                      g_arb_vertex_program_str);

   GLuint progid_frag;
   glGenProgramsARB(1, &progid_frag);
   glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, progid_frag);
   glProgramStringARB(GL_FRAGMENT_PROGRAM_ARB, GL_PROGRAM_FORMAT_ASCII_ARB,
                      strlen(g_arb_fragment_program_str),
                      g_arb_fragment_program_str);

   glClearColor(1.f, 0.f, 0.f, 1.f);

   double params1[] = {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
   glGetDoublev(GL_MODELVIEW_MATRIX, params1);

   double params2[] = {0.0025, 0, 0,      0, 0,  -0.00333333, 0, 0,
                       0,      0, -0.001, 0, -1, 1,           0, 1};
   glGetDoublev(GL_PROJECTION_MATRIX, params2);

   glProgramLocalParameter4dARB(GL_VERTEX_PROGRAM_ARB, 1, 0.0025, 0, 0, -1);
   glProgramLocalParameter4dARB(GL_VERTEX_PROGRAM_ARB, 2, 0, -0.00333333, 0,
                                1);
   glProgramLocalParameter4dARB(GL_VERTEX_PROGRAM_ARB, 3, 0, 0, -0.001, 0);
   glProgramLocalParameter4dARB(GL_VERTEX_PROGRAM_ARB, 4, 0, 0, 0, 1);

   while (true) {
      {
         glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
         glActiveTexture(GL_TEXTURE0);
         glBindTexture(GL_TEXTURE_2D, texture_rgba);
         glEnable(GL_TEXTURE_2D);


         glBegin(GL_QUADS);

         glMultiTexCoord3f(GL_TEXTURE0, 0, 1, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(0, 0, 40);

         glMultiTexCoord3f(GL_TEXTURE0, 1, 1, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(800, 0, 40);

         glMultiTexCoord3f(GL_TEXTURE0, 1, 0, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(800, 600, 40);

         glMultiTexCoord3f(GL_TEXTURE0, 0, 0, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(0, 600, 40);

         glEnd();

         glfwSwapBuffers(window);
         glfwPollEvents();
      }

      std::cin.get();

      {
         glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
         glActiveTexture(GL_TEXTURE0);
         glBindTexture(GL_TEXTURE_2D, texture_depth);
         glEnable(GL_TEXTURE_2D);

         glBegin(GL_QUADS);

         glMultiTexCoord3f(GL_TEXTURE0, 0, 1, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(0, 0, 40);

         glMultiTexCoord3f(GL_TEXTURE0, 1, 1, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(800, 0, 40);

         glMultiTexCoord3f(GL_TEXTURE0, 1, 0, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(800, 600, 40);

         glMultiTexCoord3f(GL_TEXTURE0, 0, 0, 0);
         glColor4f(1, 1, 1, 1);
         glVertex3f(0, 600, 40);

         glEnd();

         glfwSwapBuffers(window);
         glfwPollEvents();
      }

      std::cin.get();
   }

   std::cin.get();
}
